# Assignment 2 - React Sign Language Translator

Second assignment in the frontend part of the Noroff accelerate course.

## Description

### Part one - Component Tree
In the first part of this assignment we made a component tree to easier see how the application would be built up and to easier see the components each view should have.

### Part two - HTML, CSS & React
In the second part of this assignment we build a react app with a login page, translation page and a profile page. The login page only takes a username and based on wether or not the username exists in the given API that we also hosted it creates a user or logs into a user. In the translation part you can translate words or sentences into sign language, when the button is pressed it will also post this to the API. We also created a profile page to display the ten last translations, a clear history button and the option to log out.

## Project status
All of the functionality is implemented, but the styling of the application and scalability on mobile devices could be improved.

## Usage
The project can either be downloaded from this repository and run in vscode with the right dependencies installed. The project is also hosted on Netlify during grading [Translation-Application](https://erikogidatranslator.netlify.app/)[![Netlify Status](https://api.netlify.com/api/v1/badges/e3b8f77c-d81d-4d76-ab45-5aefb19913b7/deploy-status)](https://app.netlify.com/sites/erikogidatranslator/deploys)

## Technologies
* HTML
* CSS
* JavaScript
* React
* Railway

## Component tree of the Translator application
![component tree](https://gitlab.com/assignment2frontend/assignment2lostintranslation/uploads/8727df7b75e3f7f18e510780e3d06cce/Component_tree.png)

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Contributers
* Ida Huun Michelsen
* Erik Aardal

### License
[MIT](https://choosealicense.com/licenses/mit/)
