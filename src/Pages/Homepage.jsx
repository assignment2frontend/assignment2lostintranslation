import Login from '../Components/Homepage/Login'

function Homepage(props) {

    return (
        <div class="homepage">
            <div id="box1">
            <img id="logoHomepage" src='Logo-Hello.png' alt='logo'/>
                <div id="box1Text">
                    <h1>Lost in Translation</h1>
                    <h2>Get started</h2>
                </div>
            </div>
            <div id="box2">
            <Login />
            </div>
        </div>
    )
}

export default Homepage