import withAuth from "../Hoc/withAuth";
import ProfileActions from "../Components/Profile/ProfileActions";
import ProfileHeader from "../Components/Profile/ProfileHeader";
import ProfileHistory from "../Components/Profile/ProfileHistory";
import { useUser } from "../Context/UserContext";
import { useEffect } from "react";
import { userById } from "../Api/user";
import { storageSave } from "../Utils/storage";
import { STORAGE_KEY_USER } from "../Const/storageKeys";


function Profile(props) {

    const { user, setUser } = useUser()

    useEffect(() => {
        const findUser = async() => {
            const [error, latestUser] = await userById(user.id)
            if (error === null ) {
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }

        findUser()
    }, [ setUser, user.id ])



    let translationHistory = user.translations.filter((val, index, arr) => index > arr.length - 10).reverse()

    return (
        <div class="profile">
            <h1 id="profileHeader">Profile</h1>
            <ProfileHeader username={ user.username } />
            <ProfileHistory translations={ translationHistory } />
            <ProfileActions />
        </div>
    )
}

export default withAuth(Profile)