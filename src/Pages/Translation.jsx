import { addTranslation } from "../Api/user";
import TranslationField from "../Components/Translation/TranslationField";
import withAuth from "../Hoc/withAuth";
import { useUser } from "../Context/UserContext"
import { storageSave } from "../Utils/storage";
import { STORAGE_KEY_USER } from "../Const/storageKeys";

function Translation(props) {

    const { user, setUser } = useUser()

    const handleTranslationClicked = async (translation) => {
        const [error, updatedUser] = await addTranslation(user, translation)
        if (error !== null) {
            return
        }
        //KEEP UI STATE AND SERVER STATE IN SYNC
        storageSave(STORAGE_KEY_USER, updatedUser)
        //UPDATE CONTEXT STATE
        setUser(updatedUser)

        console.log(updatedUser)
    }

    return (
        <div class="translation">
            <h1>Translate</h1>
            <h3>In this field you can enter text you want to translate to sign language :)</h3>
            <p>ps. the arrow button needs to be pressed to translate</p>
            <section id="translations">
                <TranslationField onTranslate={ handleTranslationClicked }/>
            </section>
        </div>
    )
}

export default withAuth(Translation)