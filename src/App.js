import './App.css';
import Homepage from './Pages/Homepage';
import Translation from './Pages/Translation';
import Profile from './Pages/Profile';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navbar from './Components/Navbar/Navbar';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/translator" element={<Translation />} />
        <Route path="/profile" element={<Profile />} />
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
