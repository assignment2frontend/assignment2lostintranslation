import { createHeaders } from './index'
const apiUrl = process.env.REACT_APP_API_URL

const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok) {
            throw new Error("Response error")
        }
        const data = await response.json()
        return [null, data]
    }
    catch(error) {
        return [error.message, []]
    }
}

const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
                })
            })
        if (!response.ok) {
            throw new Error("Could not create user with username " + username)
        }
        const data = await response.json()
        return [null, data]
    }
    catch(error) {
        return [error.message, []]
    }

}

export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username)

    if (checkError !== null) {
        return [checkError, null]
    }

    if(user.length > 0) {
        return [ null, user.pop()]
    }
    
    return await createUser(username)
}

export const userById = async(userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`)
        if (!response.ok) {
            throw new Error("Could not fetch user data")
        }
        const user = await response.json()
        return [null, user]
    } catch (error) {
        return [error.message, null]
    }
}

export const addTranslation = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                username: user.username,
                translations: [...user.translations, translation]
            })
        })
        if (!response.ok) {
            throw new Error("Could not add the translation")
        }
        const result = await response.json()
        return [null, result]
    }
    catch (error){
        return [error.message, null]
    }
}

export const clearTranslations = async(userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error("Could not clear history")
        }
        const result = await response.json()
        return [ null, result ]
    } catch (error) {
        return [error.message, null]
    }
}