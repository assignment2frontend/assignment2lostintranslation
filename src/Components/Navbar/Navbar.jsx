import { NavLink } from "react-router-dom"
import { useUser } from "../../Context/UserContext"

const Navbar = () => {

    const { user } = useUser()

    return (
        <nav>
            <ul>
                <li><h2>Lost in translation</h2></li>
            </ul>
            { user !== null &&
            <>
            <ul>
                <li><NavLink to="/translator"><img id="logoImage" src="Logo.png" width="100" alt="translator"></img></NavLink></li>
            </ul>
            <ul>
                <li><NavLink to="/profile"><img src="profile.png" width="100" alt="profile"></img></NavLink></li>
                <p id="navUsername">{user.username}</p>
            </ul>
            </>
            }
        </nav>
    )
}
export default Navbar