import ProfileHistoryItem from "./ProfileHistoryItem"

const ProfileHistory = ({translations}) => {

    const translationList = translations.map(
        (translation, index) => <ProfileHistoryItem key={index} translation={translation}/>)

    return (
        <section id="translationHistory">
            <h2>Translation history:</h2>
            <ul>
                {translationList}
            </ul>
        </section>
    )
}
export default ProfileHistory