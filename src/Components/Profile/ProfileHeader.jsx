const ProfileHeader = ({ username }) => {
    return (
        <header>
            <h3 id="welcomeText">Hello, welcome back <p id="welcomeUser">{username}</p></h3>
        </header>
    )
}
export default ProfileHeader