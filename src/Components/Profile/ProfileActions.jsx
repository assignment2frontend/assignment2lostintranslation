import { STORAGE_KEY_USER } from "../../Const/storageKeys";
import { storageDelete, storageSave } from "../../Utils/storage";
import { useUser } from "../../Context/UserContext";
import { clearTranslations } from "../../Api/user";

const ProfileActions = () => {

    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if (window.confirm("Are you sure?")) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }
    const handleClearHistoryClick = async() => {
        if(!window.confirm("Are you sure?\nThis can not be undone!")) {
            return
        }

        const [clearError] = await clearTranslations(user.id)

        if (clearError !== null) {
            return
        }
        const updatedUser = {
            ...user,
            translations: []
        }
        storageSave(STORAGE_KEY_USER,updatedUser)
        setUser(updatedUser)
    }

    return (
        <ul class="buttons">
            <li><button id="clearHistoryButton" onClick={handleClearHistoryClick}>Clear history</button></li>
            <li><button id="logoutButton"onClick={handleLogoutClick}>Log out</button></li>
        </ul>
    )
}
export default ProfileActions