import { useForm } from "react-hook-form"
import TranslationImage from "./TranslationImage"

export let lettersToImage = []

const TranslationField = ( { onTranslate }) => {

    const { register, handleSubmit} = useForm()

    let letters = ''

    const onSubmit = ({translation}) => {
        letters = translation
        lettersToImage = letters.split('')
        onTranslate(translation)
    }

    const handleChars = e => {
        const {char, value} = e.target
        console.log([char] + ' ' + value)
    }

    return (
        <>
        <form onKeyDown={(e) => { e.key === 'Enter' && e.preventDefault() }} onSubmit={handleSubmit(onSubmit)}>
            <fieldset id="translationField">
                <label htmlFor="translation"></label>
                <input type="text"
                 {...register('translation') }
                  placeholder="⌨ | Type something here ..."
                  onChange={handleChars} />
                <button type="submit">➡</button>
            </fieldset>
        </form>
        <fieldset id="imageField">
            <TranslationImage />
        </fieldset>
        </>
    )
}
export default TranslationField