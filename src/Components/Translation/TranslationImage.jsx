import { lettersToImage } from "./TranslationField";

const TranslationImage = () => {
    console.log(lettersToImage)
    var renderedLetters = lettersToImage.map((char, index) => {
        return (
          <div id="images" key={index}>
            <img src={`individual_signs/${char}.png`} alt={char} />
          </div>
        );
      });
    
    return (
        <>
        {renderedLetters}
        </>
    )
}

export default TranslationImage