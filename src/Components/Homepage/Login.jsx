import { useForm } from 'react-hook-form'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { loginUser } from '../../Api/user'
import { useUser } from '../../Context/UserContext'
import { storageSave } from '../../Utils/storage'
import { STORAGE_KEY_USER } from '../../Const/storageKeys'

const usernameConfig = {
    required: true,
    minLength: 3
}

const Login = () => {

    const {register, handleSubmit, formState: { errors }} = useForm()
    const { user, setUser } = useUser()

    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)
    const navigate = useNavigate()

    useEffect(() => {
        if(user !== null) {
            navigate("/translator")
        }
    }, [user, navigate])

    const onSubmit = async ({username}) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false);
    }

const errorMessage = (() => {
    if (!errors.username) {
        return null
    }
    if (errors.username.type === 'required') {
        return <p>You need a name to continue</p>
    }
    if (errors.username.type === 'minLength') {
        return <p>You need to enter a longer name (min 3)</p>
    }
})()

    return (
        <>
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset>
                <label htmlFor="username"></label>
                <input type="text" placeholder="⌨ | What is your name?"{...register("username", usernameConfig)} />
                <button type="submit" disabled={ loading }>➡</button>
            </fieldset>
            {loading && <p>Logging in ...</p>}
            { errorMessage }
            {apiError && {apiError}}
        </form>
        </>
    )
}

export default Login